# Vroombot

## What is it?

Vroombot is a Picobot derivative.
Picobot was originally designed by the introduction to computer science course designers of Harvey Mudd College. This is a reimaging of their design with more modern techniques and a simpler syntax. It uses a visual language that hints at the nature of programming syntax, without the written syntax getting in the way.

## How does it work?

Vroombot is a robot that can move in any cardinal direction. It chooses where to move based upon rules written by the player. These rules are organized by a grouping that is designated by a value that represents Vroombot's state. In fact, this state is the only memory Vroombot has. The idea of the puzzle is to write rules that are sufficient for covering all tiles by navigating around walls regardless of where Vroombot starts, since the bot will start in a random tile each time.

## What does it teach?

Vroombot, like Picobot before it, teaches the concept of computation early without the need for a sophisticated language. The current Vroombot is not turing complete, but is sufficiently complicated as to present a computational challenge to any student regardless of previous experience. 

Since Vroombot is described by a set of rules, it is easy to show complexity as a factor of the number of rules. For instance, although several solutions are equally correct, some are more efficient since they involve less rules (space complexity), or solve the puzzle with Vroombot doing less repetitive work (time complexity).

## Development

To build VroomBot, install `npm` and `git` and then download this repository:

```shell
git clone https://gitlab.com/wilkie/vroombot
```

Navigate to the vroombot directory and install the dependencies using `npm`:

```shell
npm install
```

Build using this command:

```shell
npm run build
```

You should have a `css/vroombot.css` and `vroombot.js` in the dist directory.

For the application itself, after building just serve the `vroombot` directory and navigate to the `app` directory using a webserver.

You can use `npm` to quickly start one:

```shell
npx http-server
```

Or python:

```shell
python -m http.server
```

In either case, look at the output to know how to navigate your browser to your local copy of VroomBot.
