"use strict";

/**
 * Represents a single PicoBot rule.
 */
export class Rule {
    // The state this belongs to.
    #state = 0;

    // The sensor triggers for each direction.
    #sensing = {};

    // The action this rule gives to the bot when it senses.
    #action = {};

    // The state this rule goes to.
    #toState = 0;

    constructor(options = {}) {
        if (options.toState == undefined) {
            options.toState = options.state || 0;
        }

        this.#state = options.state || 0;
        this.#sensing = options.sensing || {};
        this.#action = options.action || "N";
        this.#toState = options.toState;
    }

    /**
     * Returns the state this rule switches to when it is satisfied.
     *
     * @return {number} The state identifier.
     */
    get toState() {
        return this.#toState;
    }

    /**
     * Updates the destination state to the given value.
     *
     * @param {number} The state identifier.
     */
    set toState(value) {
        this.#toState = value;
    }

    /**
     * Returns the state this rule belongs to.
     *
     * @return {number} The state identifier.
     */
    get state() {
        return this.#state;
    }

    /**
     * Returns the sensor state this rule is checking for.
     *
     * @return {object} The sensor state data.
     */
    get sensing() {
        return Object.assign(this.#sensing, {});
    }

    /**
     * Returns the action to be performed when the rule is satisfied.
     */
    get action() {
        return this.#action;
    }

    /**
     * Sets the action this rule takes.
     */
    set action(value) {
        this.#action = value;
    }

    /**
     * Sets that this rule acts on the given sense.
     *
     * @param {string} sense - The sense key.
     */
    addSense(sense) {
        this.#sensing[sense] = true;
    }

    /**
     * Removes the given sense from consideration.
     *
     * It will no longer act on that sense.
     *
     * @param {string} sense - The sense key.
     */
    removeSense(sense) {
        delete this.#sensing[sense];
    }

    /**
     * Iterates through each sensing rule.
     */
    forEach(...args) {
        return Object.keys(this.#sensing).map( (key) => {
            return { key: key, value: this.#sensing[key] };
        }).forEach(...args);
    }

    /**
     * Iterates through each sensing rule, breaking when the callback returns true.
     */
    some(...args) {
        return Object.keys(this.#sensing).map( (key) => {
            return { key: key, value: this.#sensing[key] };
        }).some(...args);
    }

    /**
     * Exports the rule to text.
     *
     * @return {string} Text representing this rule.
     */
    exportText() {
        return `${this.#state} ${Object.keys(this.#sensing).join('')} -> ${this.action} ${this.toState}`;
    }

    /**
     * Returns true if this rule matches the given sensing data.
     */
    matches(sensing) {
        let ret = true;
        Object.keys(this.#sensing).some( (key) => {
            if (!sensing[key]) {
                ret = false;
                return false;
            }
        });
        return ret;
    }
};
