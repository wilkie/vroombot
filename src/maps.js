export class Maps {
}

/**
 * The default VroomBot map.
 */
Map.DEFAULT_MAP = `
# name: The Amazing Maze
xxxxxxxxxxxxxxxxxxxxxxxxx
x..x.x...x...x...x...xx.x
xx.x.x.x.x.x.x.x...x....x
xx.x.xxx.x.x.x.xxxxxxxx.x
x..x.....x.x.x...x....x.x
x.xxx.xxxx.x.x.x.x.xx...x
x..........x.x.x.x.xxxxxx
xxxxxxxxxxxx.xxx.x..x...x
x..x...x...x...x.xx.x.xxx
xx.x.x...x.x.xxx..x.x...x
xx.x.xxxxx.x.x.x.xx.x.x.x
xx.x.....x...x....x.xxx.x
xx.xxxxx.xx.xxxxx.x.....x
x......x.xx.....x.xxx.x.x
x.xxxxxx.x..xxx.x...x.x.x
x........x.xx.x.xxx.x.x.x
xxxxxxxxxx.x..x...x.x.x.x
x....x...xxx.xxxx.x.x.x.x
x.xx.x.x........x.x.xxx.x
x..x.x.xxxxx.xx.x.x.x.x.x
x.xx.x.x.x...x..x...x...x
x.x..x.x...xxx.xxxxxxx.xx
x.xxxx.x.x...x.x.....x.xx
x......x.xxx.x...xxx...xx
xxxxxxxxxxxxxxxxxxxxxxxxx`

/**
 * The default VroomBot map.
 */
Map.DEFAULT_MAP = `
# name: The Ballroom
xxxxxxxxxxxxxxxxxxxxxxxxx
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
x.......................x
xxxxxxxxxxxxxxxxxxxxxxxxx`
