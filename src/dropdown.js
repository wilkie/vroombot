"use strict";

/**
 * This represents a dropdown menu widget.
 */
export class Dropdown {
    // The actual dropdown list element.
    #dropdownElement = null;

    // The button element
    #buttonElement = null;

    constructor(element, label, items) {
        // Retain button element
        this.#buttonElement = element;

        // Create the dropdown menu element
        this.#dropdownElement = document.createElement("ul");
        this.#dropdownElement.classList.add("vroombot-dropdown");

        // Set ARIA properties
        this.#dropdownElement.setAttribute("role", "menu");
        this.#dropdownElement.setAttribute("aria-label", label);

        element.setAttribute('title', label);

        // Add each dropdown menu item
        items.forEach( (item) => { this.add(item); });

        // Make sure the button element is ARIA configured
        element.setAttribute("aria-haspopup", "true");
        element.setAttribute("aria-expanded", "false");

        this.#dropdownElement.style.display = "none";
        this.#dropdownElement.addEventListener("blur", this.#blurEvent.bind(this));

        // Insert the dropdown after the button
        element.parentNode.insertBefore(this.#dropdownElement, element.nextSiblingElement);

        // Handle opening the dropdown
        element.addEventListener("mousedown", this.#clickEvent.bind(this));
        element.addEventListener("mouseup", this.#mouseUpEvent.bind(this));
    }

    add(item) {
        let li = document.createElement("li");
        li.classList.add("vroombot-dropdown-item");
        li.setAttribute("role", "none");
        let button = document.createElement("button");
        button.setAttribute("role", "menuitem");
        button.textContent = item.label;
        button.classList.add("vroombot-dropdown-button");
        button.classList.add(item.name);

        button.addEventListener("mouseup", this.#itemClickEvent.bind(this, item));

        li.appendChild(button);
        this.#dropdownElement.appendChild(li);
    }

    open() {
        let button = this.#buttonElement;
        let dropdown = this.#dropdownElement;

        dropdown.style.display = "block";

        // Position the dropdown appropriately
        let bbox = button.getBoundingClientRect();
        let top = button.offsetHeight + bbox.top;
        let left = bbox.left;

        // Re-adjust if it falls off the screen
        if (bbox.top + dropdown.offsetHeight > window.innerHeight) {
            // Place it on top instead
            top = bbox.top - dropdown.offsetHeight;
        }

        if (bbox.left + dropdown.offsetWidth > window.innerWidth) {
            // Place it flush on the window edge
            left = window.innerWidth - dropdown.offsetWidth;
        }

        dropdown.style.left = left + "px";
        dropdown.style.top = top + "px";

        dropdown.setAttribute("tabindex", "0");
        dropdown.focus();

        event.preventDefault();
        event.stopPropagation();
    }

    close() {
        this.#dropdownElement.style.display = "";
    }

    /**
     * Handles the click on the dropdown button to reveal the list.
     */
    #clickEvent(event) {
        this.open();
    }

    /**
     * Handles the mouseup event on the dropdown button.
     */
    #mouseUpEvent(event) {
    }

    /**
     * Handles the blur event when something else was clicked on.
     */
    #blurEvent(event) {
        // Detect if focus is transferred to something within ourselves
        if (event.relatedTarget && event.relatedTarget.parentNode &&
            event.relatedTarget.parentNode.parentNode &&
            event.relatedTarget.parentNode.parentNode.parentNode) {
            if (event.relatedTarget.parentNode === event.target ||
                event.relatedTarget.parentNode.parentNode === event.target ||
                event.relatedTarget.parentNode.parentNode.parentNode === event.target) {
                return;
            }
        }

        this.close();
    }

    /**
     * Handles click events on items.
     */
    #itemClickEvent(item, event) {
        item.callback();

        // Close list
        this.close();
    }
}
