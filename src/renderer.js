"use strict";

import elementResizeEvent from "element-resize-event";

/**
 * This renders a given map to the established HTMLElement.
 */
export class Renderer {
    // An instance of the Map we are currently using.
    #map = null;

    // An instance of the Bot we are currently using.
    #bot = null;

    // Keeping track of where the bot currently is this frame.
    #botPosition = {left: 0, top: 0};

    // The main element we are rendering within.
    #element = null;

    // The current zoom of the map.
    #zoom = 20;

    // Time in milliseconds it takes to animate one step of a program.
    #speed = 10;

    /**
     * Constructs a new renderer that will render content into the given element.
     *
     * @param {HTMLElement} element - The element to place the contents within.
     * @param {object} options - Renderer options.
     */
    constructor(element, options = {}) {
        this.#element = element;
        this.#map = options.map || null;
        this.#speed = options.speed || 100;

        elementResizeEvent(this.#element, () => { this.resize() });
    }

    /**
     * Returns the current animation speed.
     */
    get speed() {
        return this.#speed;
    }

    /**
     * Sets the speed of the machine.
     *
     * @param {number} value - The time in milliseconds for each step of the program.
     */
    set speed(value) {
        this.#speed = value;
    }

    /**
     * Returns the current vroombot.
     */
    get bot() {
        return this.#bot;
    }

    /**
     * Sets the vroombot being used.
     *
     * @param {Bot} bot - The instance of the Bot to render.
     */
    set bot(bot) {
        this.#bot = bot;
        this.#botPosition = { left: this.#bot.x, top: this.#bot.y };
    }

    /**
     * Returns the current map.
     *
     * @return {Map} The current map or null if there is none.
     */
    get map() {
        return this.#map;
    }

    /**
     * Loads the given map into the machine.
     *
     * @param {Map} map - The map to render.
     */
    set map(map) {
        this.#map = map;

        // Find the existing container for the map
        let containerElement = this.#element.querySelector(".vroombot-map-container");

        // Create a map container
        if (!containerElement) {
            containerElement = document.createElement("div");
            containerElement.classList.add("vroombot-map-container");
            this.#element.appendChild(containerElement);
        }

        // Clear out the elements to start
        containerElement.innerHTML = "";

        let mapTitleElement = document.createElement("div");
        mapTitleElement.classList.add("vroombot-map-title");

        let mapTitleCaptionElement = document.createElement("div");
        mapTitleCaptionElement.classList.add("vroombot-map-title-caption");
        mapTitleCaptionElement.textContent = this.#map.name;
        mapTitleElement.appendChild(mapTitleCaptionElement);

        containerElement.appendChild(mapTitleElement);

        // Create image elements for this map
        for (let y = 0; y < this.#map.height; y++) {
            let rowElement = document.createElement("div");
            rowElement.classList.add("vroombot-map-row");
            rowElement.setAttribute("data-y", y);
            containerElement.appendChild(rowElement);

            for (let x = 0; x < this.#map.width; x++) {
                let image = document.createElement("img");
                image.classList.add("vroombot-tile");
                image.setAttribute("data-x", x);
                image.setAttribute("data-y", y);
                image.setAttribute("draggable", false);
                rowElement.appendChild(image);
            }
        }

        // Add a Bot
        let image = document.createElement("img");
        image.src = 'img/red.gif';
        image.classList.add("vroombot-bot");
        image.setAttribute("draggable", false);
        containerElement.appendChild(image);

        this.resize();
    }

    /**
     * Returns the current element attached to the renderer.
     *
     * @return {HTMLElement} The element in which the content is rendered.
     */
    get element() {
        return this.#element;
    }

    /**
     * Resizes the images to fit the available space.
     */
    resize() {
        let width = this.#element.clientWidth * 0.9;
        let height = this.#element.clientHeight * 0.9;

        // Fit the map into the client area by resizing the tiles.
        let mapWidth = this.#map.width;
        let mapHeight = this.#map.height;

        let extent = Math.round(Math.min(width / mapWidth, height / mapHeight));

        this.#element.querySelectorAll("img.vroombot-tile, img.vroombot-bot").forEach( (image) => {
            image.style.width = extent + "px";
        });

        this.#zoom = extent;

        this.update();
    }

    /**
     * Renders the map.
     */
    render() {
        if (!this.#map) {
            return;
        }

        // Render the map
        for (let y = 0; y < this.#map.height; y++) {
            for (let x = 0; x < this.#map.width; x++) {
                let imageURL = "img/empty_uncleared.png";
                if (this.#map.isWall(x, y)) {
                    let wallName = this.#chooseWall(x, y);
                    imageURL = `img/wall_${wallName}.png`;
                }

                let image = this.#element.querySelector(`img[data-x="${x}"][data-y="${y}"]`);
                if (image && image.src != imageURL) {
                    image.src = imageURL;
                }
            }
        }

        let containerElement = this.#element.querySelector(".vroombot-map-container");

        if (!this.#map.victorious) {
            containerElement.classList.remove("vroombot-victory");
        }

        if (!this.#map.vanquished) {
            containerElement.classList.remove("vroombot-defeat");
        }

        // Move the bot
        if (this.#bot) {
            let botElement = this.#element.querySelector("img.vroombot-bot");
            let x = this.#bot.x * this.#zoom;
            let y = this.#bot.y * this.#zoom;

            botElement.style.left = (x - 1) + "px";
            botElement.style.top = (y - 1) + "px";
        }

        this.#botPosition = { left: this.#bot.x, top: this.#bot.y };
    }

    /**
     * Updates the current state of the map and bot.
     */
    async update() {
        let newPosition = { left: this.#bot.x, top: this.#bot.y };

        // Animate the little robot
        let botElement = this.#element.querySelector("img.vroombot-bot");
        let property = "left";
        let locked = "top";
        let scale = 1;
        if (newPosition.top != this.#botPosition.top) {
            property = "top";
            locked = "left";
            if (newPosition.top < this.#botPosition.top) {
                scale = -1;
            }
        }
        else if (newPosition.left < this.#botPosition.left) {
            scale = -1;
        }

        let botStart = parseInt(botElement.style[property]);
        let start = 0; 

        if ((newPosition.left != this.#botPosition.left ||
             newPosition.top != this.#botPosition.top) &&
            this.speed > 0) {
            let oldPosition = this.#botPosition;
            await new Promise( (resolve, reject) => {
                let animateBot = (timestamp) => {
                    if (!start) start = timestamp;
                    var progress = timestamp - start;

                    botElement.style[property] = ((oldPosition[property] * this.#zoom) + (Math.min((progress / this.#speed) * this.#zoom, this.#zoom)) * scale) + 'px';
                    botElement.style[locked] = (oldPosition[locked] * this.#zoom) + "px";

                    if (progress < this.#speed) {
                        window.requestAnimationFrame(animateBot);
                    }
                    else {
                        resolve();
                    }
                };

                window.requestAnimationFrame(animateBot);
            });
        }
        else {
            // Stepping... (or it is updating too quickly) just move it instantaneously
            let x = this.#bot.x * this.#zoom;
            let y = this.#bot.y * this.#zoom;

            botElement.style.left = (x - 1) + "px";
            botElement.style.top = (y - 1) + "px";
        }

        // Update tile image
        let tile = this.#map.get(this.#bot.x, this.#bot.y);
        let image = this.#element.querySelector(`img[data-x="${this.#bot.x}"][data-y="${this.#bot.y}"]`);
        let imageURL = "img/empty_cleared.png";
        if (image && image.src != imageURL) {
            image.src = imageURL;
        }
        this.#map.set(this.#bot.x, this.#bot.y, ' ');

        this.#botPosition = newPosition;

        if (this.#map.victorious) {
            let containerElement = this.#element.querySelector(".vroombot-map-container");
            containerElement.classList.add("vroombot-victory");
        }
        else if (this.#map.vanquished) {
            let containerElement = this.#element.querySelector(".vroombot-map-container");
            containerElement.classList.add("vroombot-defeat");
        }
    }

    /**
     * Internal function to choose the tile graphic name.
     */
    #chooseWall(x, y) {
        let wallIndex = 0;

        let wallTop = this.#map.isWall(x, y - 1);
        let wallBottom = this.#map.isWall(x, y + 1);
        let wallLeft = this.#map.isWall(x - 1, y);
        let wallRight = this.#map.isWall(x + 1, y);

        let wallTopLeft = this.#map.isWall(x - 1, y - 1);
        let wallTopRight = this.#map.isWall(x + 1, y - 1);
        let wallBottomLeft = this.#map.isWall(x - 1, y + 1);
        let wallBottomRight = this.#map.isWall(x + 1, y + 1);

        if (!wallLeft) {
            wallIndex += 1;
        }
        else if (wallBottom && !wallBottomLeft) {
            wallIndex += 2;
        }
        if (!wallBottom) {
            wallIndex += 10;
        }
        else if (wallRight && ! wallBottomRight) {
            wallIndex += 20;
        }
        if (!wallRight) {
            wallIndex += 100;
        }
        else if (wallTop && !wallTopRight) {
            wallIndex += 200;
        }
        if (!wallTop) {
            wallIndex += 1000;
        }
        else if (wallLeft && !wallTopLeft) {
            wallIndex += 2000;
        }

        return ("" + wallIndex).padStart(4, '0');
    }
};
