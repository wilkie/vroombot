"use strict";

import { __ } from 'i18n-for-browser';
import { format } from './i18n.js';
import PerfectScrollbar from 'perfect-scrollbar';

import { State } from './state.js';
import { Rule } from './rule.js';
import { Dropdown } from './dropdown.js';

/**
 * This represents the rules editor widget.
 *
 * This widget allows graphical editing of rules for the robot.
 */
export class RulesEditor {
    // The HTMLElement
    #element = null;

    // The <ul> containing the actual list of states.
    #statesElement = null;

    // A convenience bucket of elements for each state.
    #states = {};

    // The Program instance we are editing.
    #program = null;

    // Keeps track of the width of the 'else if' entry.
    #elseIfWidth = null;

    // Keeps track of which rule is highlighted
    #highlighted = null;

    /**
     * Constructs a new editor widget that will render content into the given element.
     *
     * @param {HTMLElement} element - The element to place the contents within.
     * @param {object} options - RulesEditor options.
     */
    constructor(element, options = {}) {
        this.#element = element;

        // Create a rules list
        let statesElement = document.createElement("ul");
        statesElement.classList.add("vroombot-states");
        this.#statesElement = statesElement;
        this.#element.appendChild(this.#statesElement);
        this.#program = options.program || null;
    }

    /**
     * Returns the current element attached to the editor widget.
     *
     * @return {HTMLElement} The element in which the content is rendered.
     */
    get element() {
        return this.#element;
    }

    /**
     * Returns the Program this widget is modifying.
     */
    get program() {
        return this.#program;
    }

    /**
     * Opens the given Program for editing.
     *
     * @param {Program} program - The Program to now edit.
     */
    set program(program) {
        this.#program = program;
        this.reset();
    }

    /**
     * Returns the Rule that is highlighted.
     *
     * @return {Rule} The currently highlighted rule, or null;
     */
    get highlighted() {
        return this.#highlighted;
    }

    /**
     * Highlights the given Rule.
     *
     * @param {Rule} rule - The Rule to highlight.
     */
    set highlighted(rule) {
        this.#highlighted = rule;

        if (rule) {
            // Find this rule.
            let stateElement = this.#states[rule.state];
            if (!stateElement) {
                return;
            }

            let state = stateElement.instance;
            let index = state.indexOf(rule)

            this.#element.querySelectorAll(`li.vroombot-rule.vroombot-rule-highlighted`).forEach( (ruleElement) => {
                ruleElement.classList.remove("vroombot-rule-highlighted");
            });

            let ruleElement = stateElement.querySelector(`li.vroombot-rule:nth-child(${index + 1})`);
            if (!ruleElement) {
                return;
            }

            ruleElement.classList.add("vroombot-rule-highlighted");
        }
    }

    /**
     * Clears out the editor.
     */
    reset() {
        this.#states = {};
        this.#statesElement.innerHTML = "";

        let stateCount = 0;
        this.#program.forEach( (state) => {
            this.addState(state);
            stateCount++;
        });

        let state = new State({ name: stateCount });
        this.addState(state);
        this.#program.addState(state);

        const ps = new PerfectScrollbar(this.#element);
    }

    /**
     * Adds the given State to the editor.
     *
     * @param {State} state - The State to add.
     */
    addState(state) {
        // Create an element to hold the state information.
        let stateElement = document.createElement("li");
        stateElement.classList.add("vroombot-state");

        // And the metadata fields
        let stateNameElement = document.createElement("span");
        stateNameElement.classList.add("vroombot-state-name");
        stateNameElement.textContent = `${__("state")} ${state.name}`;
        stateElement.appendChild(stateNameElement);

        // Add a rules list
        let rulesElement = document.createElement("ol");
        rulesElement.classList.add("vroombot-rules");
        stateElement.appendChild(rulesElement);

        // Add a "new rule" button
        let addButtonElement = document.createElement("button");
        addButtonElement.classList.add("vroombot-add-rule");
        addButtonElement.textContent = "+";
        stateElement.appendChild(addButtonElement);

        addButtonElement.addEventListener("click", (_) => {
            // Only add a rule if there isn't a blank one already in the state
            if (!stateElement.querySelector("li.vroombot-sense:first-child > button:not([data-sensing])")) {
                let newRule = new Rule({ state: state.name });
                this.addRule(newRule);
                state.addRule(newRule);
            }

            // If we are the last child somehow... add another state as well
            if (stateElement === stateElement.parentNode.querySelector("li.vroombot-state:last-child")) {
                let stateCount = stateElement.parentNode.querySelectorAll("li.vroombot-state").length;
                let state = new State({ name: stateCount });
                this.addState(state)
                this.#program.addState(state);
            }
        });

        // Append it to the document
        this.#statesElement.appendChild(stateElement);

        this.#states[state.name] = stateElement;
        stateElement.instance = state;

        // Add "toState" options to every existing rule
        this.#element.querySelectorAll("li.vroombot-rule").forEach( (ruleElement) => {
            let toStateDropdown = ruleElement.toStateDropdown;
            let toStateButton = ruleElement.querySelector("button.vroombot-to-state");
            let rule = ruleElement.instance;

            toStateDropdown.add({
                label: format(__(`rule-switch-state`), state.name),
                name: `vroombot-to-state-${state.name}`,
                callback: () => {
                        // Update state for this rule
                        rule.toState = parseInt("" + state.name);

                        // Update data
                        toStateButton.setAttribute("data-to-state", rule.toState);
                    }
                });
        });

        state.forEach( (rule) => {
            this.addRule(rule);
        });
    }

    /**
     * Adds the given Rule to the editor.
     *
     * It will add a new state if the state belonging the rule does not exist.
     *
     * @param {Rule} rule - The Rule to add.
     */
    addRule(rule) {
        let stateElement = this.#states[rule.state];
        if (!stateElement) {
            // Create the state
            let state = new State({ name: rule.state });
            state.addRule(rule);
            this.addState(state);
            this.#program.addState(state);
            stateElement = this.#states[state.name];
        }

        let state = stateElement.instance;

        // Add element
        let ruleElement = document.createElement("li");
        ruleElement.classList.add("vroombot-rule");
        ruleElement.instance = rule;

        let ifSpan = document.createElement("span");
        ifSpan.classList.add("vroombot-if");
        ifSpan.textContent = __("if");
        if (this.#elseIfWidth) {
            ifSpan.style.width = this.#elseIfWidth + "px";
        }
        ruleElement.appendChild(ifSpan);
        if (stateElement.querySelector("ol.vroombot-rules li.vroombot-rule")) {
            ifSpan.classList.add("vroombot-else-if");
            ifSpan.innerHTML = __("else") + "<br>" + __("if");
        }

        let sensingList = document.createElement("ul");
        sensingList.classList.add("vroombot-sensing");

        let ruleCount = 0;
        rule.forEach( (sensingItem, i) => {
            if (i < 4) {
                let senseElement = document.createElement("li");
                senseElement.classList.add("vroombot-sense");
                let senseButton = document.createElement("button");
                senseButton.setAttribute("data-sensing", sensingItem.key);
                senseElement.appendChild(senseButton);
                sensingList.appendChild(senseElement);
            }
            ruleCount++;
        });

        for (let i = ruleCount; i < 4; i++) {
            let senseElement = document.createElement("li");
            senseElement.classList.add("vroombot-sense");
            let senseButton = document.createElement("button");
            let senseImage = document.createElement("img");
            senseButton.appendChild(senseImage);
            senseElement.appendChild(senseButton);
            sensingList.appendChild(senseElement);
        }

        // Now go through and add dropdowns for every sense rule.
        sensingList.querySelectorAll("li.vroombot-sense > button").forEach( (button) => {
            let dropdown = new Dropdown(button, __("rule-modify-sense"),
                "NWSEnwse ".split('').map( (key) => {
                    if (key == " ") {
                        return { label: __(`rule-clear-sense`), name: `vroombot-clear-sense`,
                            callback: () => {
                                let oldKey = button.getAttribute('data-sensing');
                                button.removeAttribute('data-sensing');
                                this.#adjustSensingList(state, rule, sensingList);
                                rule.removeSense(oldKey);
                            }
                        };
                    }
                    else {
                        return { label: __(`rule-sense-${key}`), name: `vroombot-sense-option-${key}`,
                            callback: () => {
                                let oldKey = button.getAttribute('data-sensing');
                                rule.removeSense(oldKey);
                                if (sensingList.querySelector(`button[data-sensing="${key}"]`)) {
                                    button.removeAttribute('data-sensing');
                                }
                                else {
                                    button.setAttribute('data-sensing', key);
                                    rule.addSense(key);
                                }

                                this.#adjustSensingList(state, rule, sensingList);
                            }
                        };
                    }
                })
            );
        });

        ruleElement.appendChild(sensingList);

        let thenSpan = document.createElement("span");
        thenSpan.classList.add("vroombot-then");
        thenSpan.textContent = __("then");
        ruleElement.appendChild(thenSpan);

        let actionButton = document.createElement("button");
        actionButton.classList.add("vroombot-action");

        let actionImage = document.createElement("img");
        actionImage.src = `img/action_${rule.action.toLowerCase()}.svg`;
        actionButton.appendChild(actionImage);

        ruleElement.appendChild(actionButton);

        // Add an actions dropdown
        let actionDropdown = new Dropdown(actionButton, __("rule-modify-action"),
            "NWSE".split('').map( (key) => {
                return { label: __(`rule-action-${key}`), name: `vroombot-action-option-${key}`,
                    callback: () => {
                        // Update action for this rule
                        rule.action = key;

                        // Update image
                        actionImage.src = `img/action_${rule.action.toLowerCase()}.svg`;
                    }
                };
            })
        );

        let toStateButton = document.createElement("button");
        toStateButton.classList.add("vroombot-to-state");
        toStateButton.setAttribute("data-to-state", rule.toState);
        let toStateSpan = document.createElement("span");
        toStateSpan.classList.add("vroombot-to-state-icon");
        toStateButton.appendChild(toStateSpan);
        ruleElement.appendChild(toStateButton);

        stateElement.querySelector("ol.vroombot-rules").appendChild(ruleElement);

        // Add an actions dropdown
        let toStateDropdown = new Dropdown(toStateButton, __("rule-modify-state"),
            Object.keys(this.#states).map( (key) => {
                return { label: format(__(`rule-switch-state`), key), name: `vroombot-to-state-${key}`,
                    callback: () => {
                        // Update state for this rule
                        rule.toState = parseInt("" + key);

                        // Update data
                        toStateButton.setAttribute("data-to-state", rule.toState);
                    }
                };
            })
        );
        ruleElement.toStateDropdown = toStateDropdown;

        if (!this.#elseIfWidth) {
            let span = stateElement.querySelector("ol.vroombot-rules li.vroombot-rule span.vroombot-else-if");
            if (span) {
                this.#elseIfWidth = span.clientWidth;
                stateElement.querySelectorAll("ol.vroombot-rules li.vroombot-rule span.vroombot-if").forEach( (span) => {
                    span.style.width = this.#elseIfWidth + "px";
                });
            }
        }
    }

    /**
     * Adjusts the sensing list.
     *
     * It makes sure the rules are all pushed to the left with no empty spaces.
     *
     * @param {HTMLElement} sensingList - The list of buttons for the senses.
     */
    #adjustSensingList(state, rule, sensingList) {
        // Look for the first empty button
        let emptyButton = sensingList.querySelector("li.vroombot-sense > button:not([data-sensing])");

        // If there is an empty button, shift over the stuff to the right of it
        if (emptyButton) {
            let currentItem = emptyButton.parentNode.nextElementSibling;

            while(currentItem) {
                let button = currentItem.querySelector("button[data-sensing]");
                if (button) {
                    // If we find 'stuff', shift that item over
                    emptyButton.setAttribute('data-sensing', button.getAttribute('data-sensing'));
                    button.removeAttribute('data-sensing');

                    // And make that item the new 'empty' item we are considering
                    emptyButton = button;
                }
                else {
                    // We reached another empty slot, which cannot happen unless
                    // it is the end of the list
                    break;
                }

                // Go to the next sensing item
                currentItem = currentItem.nextElementSibling;
            }
        }

        // If the list is now empty, remove the rule
        if (!sensingList.querySelector("li.vroombot-sense > button[data-sensing]")) {
            // The list is empty; remove the rule.
            state.removeRule(rule);

            let ruleElement = sensingList.parentNode;
            let rulesElement = ruleElement.parentNode;
            ruleElement.remove();

            // Alter the "if"/"else if" text.
            let ifSpan = rulesElement.querySelector("li.vroombot-rule:first-child .vroombot-if");
            if (ifSpan) {
                ifSpan.textContent = __("if");
                ifSpan.classList.remove("vroombot-else-if");
            }

            // TODO: Remove the state, if need be
            if (!rulesElement.querySelector("li.vroombot-rule")) {
                let stateElement = rulesElement.parentNode;

                // Only remove it if it is not the last state
                if (stateElement !== stateElement.parentNode.querySelector("li.vroombot-state:last-child")) {
                    //console.log("REMOVE STATE");
                }
            }
        }
    }
};
