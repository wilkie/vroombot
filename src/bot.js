"use strict";

/**
 * This represents our hero, the bot.
 */
export class Bot {
    #map = null;
    #x = 0;
    #y = 0;

    /**
     * Constructs a new Bot based on the given options.
     */
    constructor(options = {}) {
    }

    /**
     * Retrieves the x coordinate of the bot.
     *
     * @return {number} The x coordinate (column) of the bot.
     */
    get x() {
        return this.#x;
    }

    /**
     * Retrieves the y coordinate of the bot.
     *
     * @return {number} The y coordinate (row) of the bot.
     */
    get y() {
        return this.#y;
    }

    /**
     * Retrieves the Map the bot is currently living in.
     *
     * @return {Map} The current Map.
     */
    get map() {
        return this.#map;
    }

    /**
     * Sets the Map the bot will be living in.
     *
     * Resets the position to (0, 0), a position which may be impossible!
     */
    set map(map) {
        this.#map = map;
        this.move(0, 0);
    }

    /**
     * Instructs the robot to move to the given tile.
     */
    move(x, y) {
        this.#x = x;
        this.#y = y;
    }

    /**
     * Reports back its surroundings.
     */
    sense() {
        // Check surroundings
        let check = [
            [this.#map.isWall.bind(this.#map), "W", "w", this.#x - 1, this.#y],
            [this.#map.isWall.bind(this.#map), "E", "e", this.#x + 1, this.#y],
            [this.#map.isWall.bind(this.#map), "N", "n", this.#x, this.#y - 1],
            [this.#map.isWall.bind(this.#map), "S", "s", this.#x, this.#y + 1]
        ];

        let ret = {};

        check.forEach( (parts) => {
            let func = parts[0];
            if (func(...parts.slice(3))) {
                ret[parts[1]] = true;
            }
            else {
                ret[parts[2]] = true;
            }
        });

        return ret;
    }

    /**
     * Performs the given actions.
     */
    act(actions) {
        actions.split('').forEach( (action) => {
            if (action == "E") {
                this.move(this.x + 1, this.y);
            }
            else if (action == "W") {
                this.move(this.x - 1, this.y);
            }
            else if (action == "S") {
                this.move(this.x, this.y + 1);
            }
            else if (action == "N") {
                this.move(this.x, this.y - 1);
            }
        });
    }
};
