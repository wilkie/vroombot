"use strict";

import { State } from './state.js';
import { Rule } from './rule.js';

/**
 * Holds the states and hence the rules for the bot.
 */
export class Program {
    // A list of states and metadata for each one, tagged by name.
    #states = {};

    #firstState = null;

    constructor(options = {}) {
    }

    /**
     * Returns an iterator function that calls the given callback for each State.
     */
    forEach(...args) {
        return Object.keys(this.#states).map( (k) => this.#states[k] ).forEach(...args);
    }

    /**
     * Returns an breaking iterator function that calls the given callback for each State.
     *
     * You need to return false to continue iterating. It stops when your callback
     * returns true.
     */
    some(...args) {
        return Object.keys(this.#states).map( (k) => this.#states[k] ).some(...args);
    }

    /**
     * Adds the given State to the program.
     *
     * @param {State} state - A State to consider as part of the program.
     */
    addState(state) {
        if (!this.#firstState) {
            this.#firstState = state;
        }
        this.#states[state.name] = state;
    }

    /**
     * Adds the given Rule to the program.
     *
     * It will add a new state if the state belonging the rule does not exist.
     *
     * @param {Rule} rule - The Rule to add.
     */
    addRule(rule) {
        let state = this.#states[rule.state];
        if (!state) {
            // Create the state
            let newState = new State({ name: rule.state });
            this.addState(newState);
            state = this.#states[newState.name];
        }

        state.addRule(rule);
    }

    /**
     * Performs the given rule and returns the action taken.
     *
     * @param {Rule} rule - The Rule to compute.
     *
     * @return {object} An object containing the action.
     */
    compute(rule) {
        return {
            action: rule.action,
            state:  this.#states[rule.toState]
        };
    }

    /**
     * Returns the action taken in this next cycle of execution.
     *
     * @param {object} sensing - An object holding the sensor state.
     *
     * @return {object} An object containing the new state of the program.
     */
    step(state, sensing) {
        let ret = {};

        if (!state) {
            state = this.#firstState;
        }

        // Find the appropriate rule in the current state.
        state.some( (rule, i) => {
            if (rule.matches(sensing)) {
                // Follow this rule!
                ret = this.compute(rule);
                ret.rule = rule;
                return true;
            }

            return false;
        });

        return ret;
    }

    /**
     * Resets the states to reflect those from the given data.
     */
    loadRules(data) {
        // Clear the current state
        this.#states = {};
        this.#firstState = null;

        // Go through each line of the rules set
        data.trim().split("\n").forEach( (line) => {
            // Get rid of line comments
            line = line.split("#")[0].trim();

            // Replace whitespace
            line = line.replace(/\s+/g, " ");

            if (line) {
                let parts = line.split(" ");
                let state = parts[0];
                let sensing = {};
                // Split into each character
                parts[1].split("").forEach( (item) => {
                    sensing[item] = true;
                });
                let action = parts[3];
                let toState = parseInt("" + parts[4]);

                let newRule = new Rule({
                    state: state,
                    sensing: sensing,
                    action: action,
                    toState: toState
                });

                this.addRule(newRule);
            }
        });
    }

    /**
     * Exports the rule set to a text document.
     *
     * @return {string} The text representing the program.
     */
    exportText() {
        return Object.keys(this.#states).map( (name) => {
            return this.#states[name].exportText();
        }).join("\n");
    }
};
