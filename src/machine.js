"use strict";

import { Renderer } from './renderer.js';
import { Program } from './program.js';
import { Bot } from './bot.js';

/**
 * This is a complete simulation, including program and display, for VroomBot.
 */
export class Machine {
    // The current Program.
    #program = null;

    // The current State of the program.
    #currentState = null;

    // The current Map.
    #map = null;

    // The Bot, our Hero.
    #bot = null;

    // The Map renderer.
    #renderer = null;

    // Whether or not the machine is running.
    #running = false;

    // Whether or not the machine is halted.
    #halted = false;

    // The current running thread
    #thread = null

    constructor(element, options = {}) {
        this.#program = options.program || null;
        this.#map = options.map || null;
        this.#renderer = new Renderer(element, options);
        this.#bot = new Bot();
        this.#renderer.bot = this.#bot;

        this.reset();
    }

    get map() {
        return this.#map;
    }

    set map(map) {
        this.#map = map;
        this.#currentState = null;
        this.#bot.map = map;
        this.#renderer.map = map;

        this.reset();
    }

    get program() {
        return this.#program;
    }

    set program(program) {
        this.#program = program;

        this.reset();
    }

    get speed() {
        return this.#renderer.speed;
    }

    set speed(value) {
        this.#renderer.speed = value;
    }

    /**
     * Returns true if the machine is currently running.
     */
    get running() {
        return this.#running;
    }

    /**
     * Returns true if the machine is halted.
     */
    get halted() {
        return this.#halted;
    }

    /**
     * Resets and initializes the machine to a starting point.
     */
    reset() {
        this.#halted = false;
        this.#currentState = null;

        if (this.#map) {
            this.#map.reset();
            let point = this.#map.randomTraversable();
            this.#bot.move(point.x, point.y);
            this.#renderer.render();
        }
    }

    /**
     * Runs the machine. Returns a promise that will indicate when the machine
     * has completed.
     *
     * It cannot be started if the machine is halted.
     *
     * @param {function} callback - Called with the instruction executed.
     *
     * @return {Promise} The asynchronous state of the execution.
     */
    start(callback) {
        if (this.#halted) {
            return;
        }

        this.#running = true;

        if (this.#thread) {
            return this.#thread;
        }

        this.#thread = new Promise( async (resolve, reject) => {
            while(this.#running) {
                let instruction = await this.step();

                // Stop when the bot crashes / goal is met
                if (this.#map.vanquished || this.#map.victorious) {
                    this.#running = false;
                    this.#halted = true;
                }

                callback(instruction);

                // Are we stepping?
                if (this.speed == 0) {
                    // Stop
                    this.pause();
                }
            }

            this.#thread = null;
            resolve();
        });

        return this.#thread;
    }
    
    /**
     * Pauses the machine.
     */
    pause() {
        this.#running = false;
    }

    /**
     * Performs a single cycle, asynchronously.
     */
    async step() {
        // Get the robot "Sense"
        let sensing = this.#bot.sense();

        if (!this.#program) {
            return {};
        }

        // Ask the program what the actions will be
        let instruction = this.#program.step(this.#currentState, sensing)

        // Render the action
        if (instruction.action) {
            this.#bot.act(instruction.action);
        }
        else {
            // No rule! Bot goes into sleep mode.
            // We are defeated!
            this.#map.vanquished = true;
        }

        if (instruction.state) {
            this.#currentState = instruction.state;
        }

        // Did the bot run into the wall???
        if (this.#map.isWall(this.#bot.x, this.#bot.y)) {
            // We are defeated!
            this.#map.vanquished = true;
        }

        await this.#renderer.update();

        return instruction;
    }
};
