"use strict";

// Vendor
import { __ } from 'i18n-for-browser';
import { saveAs } from 'file-saver';
import fileDialog from 'file-dialog';

import { I18n } from './i18n.js';
import { State } from './state.js';
import { Map } from './map.js';
import { Bot } from './bot.js';
import { Renderer } from './renderer.js';
import { RulesEditor } from './rules-editor.js';
import { Machine } from './machine.js';
import { Program } from './program.js';
import { Dropdown } from './dropdown.js';

/**
 * The base VroomBot class which instantiates the Karel.
 */
export class VroomBot {
    // The current map Renderer object.
    #renderer = null;

    // The current RulesEditor object.
    #rulesEditor = null;

    // The current Map.
    #map = null;

    // The current State.
    #state = null;

    // The machine itself.
    #machine = null;

    // Our hero, the vroombot
    #bot = null;

    // The current Program we will be running.
    #program = null;

    // The collection of states.
    #states = [];

    constructor(options = {}) {
        // Load a default map
        this.#map = new Map();

        let vroombotElement = document.querySelector("#vroombot");
        if (vroombotElement) {
            // Ensure it has a 'vroombot' class
            vroombotElement.classList.add("vroombot");

            // Create a header
            let headerElement = document.createElement("nav");
            headerElement.classList.add("vroombot-header");
            vroombotElement.appendChild(headerElement);

            let titleSpan = document.createElement("span");
            titleSpan.textContent = "VROOM";
            headerElement.appendChild(titleSpan);

            let titleSpan2 = document.createElement("span");
            titleSpan2.textContent = "BOT";
            titleSpan2.classList.add("vroombot-bold");
            headerElement.appendChild(titleSpan2);

            let infoButton = document.createElement("button");
            infoButton.classList.add("vroombot-info");
            //headerElement.appendChild(infoButton);

            let codeButton = document.createElement("a");
            codeButton.classList.add("vroombot-code");
            codeButton.setAttribute("href", "https://gitlab.com/wilkie/vroombot");
            headerElement.appendChild(codeButton);

            // Create a toolbar for the rules pane
            let rulesActions = document.createElement("ul");
            rulesActions.classList.add("vroombot-rules-actions");

            // Create Files button
            let rulesActionsFiles = document.createElement("li");
            let rulesActionsFilesButton = document.createElement("button");
            rulesActionsFilesButton.classList.add("vroombot-rules-actions-files");
            rulesActionsFiles.appendChild(rulesActionsFilesButton);
            rulesActions.appendChild(rulesActionsFiles);

            // Create Files dropdown options
            let rulesActionsFilesDropdown = new Dropdown(rulesActionsFilesButton, __("rules-files"), [
                { label: __("rules-open"), name: "vroombot-rules-actions-files-open",
                  callback: () => {
                      fileDialog({ accept: 'text/*' }).then( (files) => {
                          let file = files[0];
                          file.text().then( (text) => {
                              this.#machine.pause();
                              mapActionsStartButton.removeAttribute("data-running");
                              this.#program.loadRules(text);
                              this.#rulesEditor.program = this.#program;
                              this.#machine.program = this.#program;
                              this.#machine.reset();
                          });
                      });
                  }
                },
                { label: __("rules-save"), name: "vroombot-rules-actions-files-save",
                  callback: () => {
                      this.exportRules();
                  }
                }
            ]);

            headerElement.appendChild(rulesActions);

            // Create a toolbar for the map pane
            let mapActions = document.createElement("ul");
            mapActions.classList.add("vroombot-map-actions");

            // Create Files button
            let mapActionsFiles = document.createElement("li");
            let mapActionsFilesButton = document.createElement("button");
            mapActionsFilesButton.classList.add("vroombot-map-actions-files");
            mapActionsFiles.appendChild(mapActionsFilesButton);
            mapActions.appendChild(mapActionsFiles);

            // Create Files dropdown options
            let mapActionsFilesDropdown = new Dropdown(mapActionsFilesButton, __("map-files"), [
                { label: __("map-open"), name: "vroombot-map-actions-files-open",
                  callback: () => {
                      fileDialog({ accept: 'text/*' }).then( (files) => {
                          let file = files[0];
                          file.text().then( (text) => {
                              this.#machine.pause();
                              mapActionsStartButton.removeAttribute("data-running");
                              this.#map = new Map({ data: text });
                              this.#machine.map = this.#map;
                              this.#machine.reset();
                          });
                      });
                  }
                },
                { label: __("map-save"), name: "vroombot-map-actions-files-save",
                  callback: () => {
                      this.exportMap();
                  }
                }
            ]);

            // Create Speed button
            let mapActionsSpeed = document.createElement("li");
            let mapActionsSpeedButton = document.createElement("button");
            mapActionsSpeedButton.classList.add("vroombot-map-actions-speed");
            mapActionsSpeedButton.setAttribute('data-speed', 'normal');
            mapActionsSpeed.appendChild(mapActionsSpeedButton);
            mapActions.appendChild(mapActionsSpeed);

            // Create Files dropdown options
            let mapActionsSpeedDropdown = new Dropdown(mapActionsSpeedButton, __("speed"), [
                { label: __("speed-step"), name: "vroombot-map-actions-speed-step",
                  callback: () => {
                      mapActionsSpeedButton.setAttribute('data-speed', 'step');
                      this.#machine.pause();
                      mapActionsStartButton.removeAttribute("data-running");
                      this.#machine.speed = 0;
                  }
                },
                { label: __("speed-slow"), name: "vroombot-map-actions-speed-slow",
                  callback: () => {
                      mapActionsSpeedButton.setAttribute('data-speed', 'slow');
                      this.#machine.speed = 3000;
                  }
                },
                { label: __("speed-normal"), name: "vroombot-map-actions-speed-normal",
                  callback: () => {
                      mapActionsSpeedButton.setAttribute('data-speed', 'normal');
                      this.#machine.speed = 100;
                  }
                },
                { label: __("speed-fast"), name: "vroombot-map-actions-speed-fast",
                  callback: () => {
                      mapActionsSpeedButton.setAttribute('data-speed', 'fast');
                      this.#machine.speed = 1;
                  }
                }
            ]);

            // Create Start button
            let mapActionsStart = document.createElement("li");
            let mapActionsStartButton = document.createElement("button");
            mapActionsStartButton.classList.add("vroombot-map-actions-start");
            mapActionsStart.appendChild(mapActionsStartButton);
            mapActions.appendChild(mapActionsStart);

            mapActionsStartButton.addEventListener("click", (_) => {
                if (this.#machine.halted) {
                    this.#machine.reset();
                    mapActionsStartButton.removeAttribute("data-stopped");
                }

                if (this.#machine.running) {
                    this.#machine.pause();
                    mapActionsStartButton.removeAttribute("data-running");
                }
                else {
                    if (this.#machine.speed > 0) {
                        mapActionsStartButton.setAttribute("data-running", "");
                    }

                    this.#machine.start( (instruction) => {
                        if (this.#machine.halted) {
                            mapActionsStartButton.removeAttribute("data-running");
                            mapActionsStartButton.setAttribute("data-stopped", "");
                        }
                        this.#rulesEditor.highlighted = instruction.rule;
                    });
                }
            });

            headerElement.appendChild(mapActions);

            // Create the container
            let containerElement = document.createElement("div");
            containerElement.classList.add("vroombot-content");
            vroombotElement.appendChild(containerElement);

            // Pre-load the cleared image
            let placeholderImage = document.createElement("img");
            placeholderImage.classList.add("vroombot-offscreen");
            placeholderImage.src = "img/empty_cleared.png";
            containerElement.appendChild(placeholderImage);

            let mapElement = document.createElement("div");
            mapElement.classList.add("vroombot-map");
            containerElement.appendChild(mapElement);

            this.#bot = new Bot();

            let rulesElement = document.createElement("div");
            rulesElement.classList.add("vroombot-rules-editor");
            containerElement.appendChild(rulesElement);

            this.#rulesEditor = new RulesEditor(rulesElement);

            // Create an initial state
            this.#state = new State();
            this.#rulesEditor.addState(this.#state);

            this.#program = new Program();
            this.#program.loadRules(`
# Current_State surroundings -> Go_Direction Go_into_state

# Capital letters for walls, lowercase letters for openings
# NeWs -> Wall to north and west, opening to east and south

# Example:

# When we are in state 0, and east is clear, don't move anywhere and go to state 1
# 0 e -> X 1 

0 e -> E 0
0 wE -> W 1
0 WE -> S 0

1 w -> W 1
1 We -> E 0

`);

            this.#rulesEditor.program = this.#program;

            this.#machine = new Machine(mapElement);
            this.#machine.map = this.#map;
            this.#machine.program = this.#program;
        }
    }

    /**
     * Prompts the browser to save a rules text file.
     */
    exportMap() {
        var blob = new Blob([this.#machine.map.exportText()],
                            { type: "text/plain;charset=utf-8" });

        saveAs(blob, "map.txt");
    }

    /**
     * Prompts the browser to save a rules text file.
     */
    exportRules() {
        var blob = new Blob([this.#machine.program.exportText()],
                            { type: "text/plain;charset=utf-8" });

        saveAs(blob, "rules.txt");
    }

    /**
     * Loads the given url as a Map.
     */
    loadMap(url) {
    }
};
