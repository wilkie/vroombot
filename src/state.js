"use strict";

/**
 * This represents a state, which is a named set of rules.
 */
export class State {
    #rules = [];
    #name = 0;

    constructor(options = {}) {
        this.#name = options.name || 0;
    }

    set name(value) {
        this.#name = value;
    }

    get name() {
        return this.#name;
    }

    /**
     * Returns an iterator function that calls the given callback for each Rule.
     */
    forEach(...args) {
        return this.#rules.forEach(...args);
    }

    /**
     * Returns an breaking iterator function that calls the given callback for each Rule.
     *
     * You need to return false to continue iterating. It stops when your callback
     * returns true.
     */
    some(...args) {
        return this.#rules.some(...args);
    }

    /**
     * Adds the given Rule to the State.
     *
     * @param {Rule} rule - The Rule to add.
     */
    addRule(rule) {
        this.#rules.push(rule);
    }

    /**
     * Removes the given rule.
     *
     * @param {Rule} rule - The rule to remove.
     */
    removeRule(rule) {
        let index = this.#rules.indexOf(rule);
        if (index >= 0) {
            this.#rules.splice(index, 1);
        }
    }

    /**
     * Returns the index of the given rule within the state.
     */
    indexOf(rule) {
        return this.#rules.indexOf(rule);
    }

    /**
     * Exports all Rules within the State to text.
     *
     * @return {string} Text representing this state.
     */
    exportText() {
        return this.#rules.map( (rule) => {
            if (Object.keys(rule.sensing).length == 0) {
                return "";
            }

            return rule.exportText();
        }).join("\n");
    }
}
