"use strict";

/**
 * This class represents a map that the robot can travel within.
 */
export class Map {
    // The tile data as an array of strings.
    #rows = [];

    // The original tile data.
    #original = [];

    // The width of the Map in units of tiles.
    #width = 0;

    // The height of the Map in units of tiles.
    #height = 0;

    // Cleared?
    #victory = false;

    // Lost?
    #defeat = false;

    // The total number of each tile.
    #totals = {};

    // The current number of each tile.
    #currentTotals = {};

    // The name of the map.
    #name = null;

    // The win condition of the map.
    // This is an object where the keys are tile types and the values are percentages.
    // The default goal is { ".": 0.0 } that is, no spaces.
    #goal = {}

    /**
     * Creates a new instance of a VroomBot Map.
     *
     * @param {MapOptions} options - The option overrides to use.
     */
    constructor(options = {}) {
        this.#importText(options.data || Map.DEFAULT_MAP);
    }

    /**
     * Returns the width of the Map.
     */
    get width() {
        return this.#width;
    }

    /**
     * Returns the height of the Map.
     */
    get height() {
        return this.#height;
    }

    /**
     * Retrieves the goal for this map.
     */
    get goal() {
        return Object.assign({}, this.#goal);
    }

    /**
     * Sets the goal for this map.
     */
    set goal(values) {
        this.#goal = values;
    }

    /**
     * Returns true if the map is considered solved.
     */
    get victorious() {
        return this.#victory;
    }

    /**
     * Returns true if the map is considered lost.
     */
    get vanquished() {
        return this.#defeat;
    }

    /**
     * Sets the losing state of the map.
     */
    set vanquished(value) {
        this.#defeat = value;
    }

    /**
     * Returns the name of the map.
     */
    get name() {
        return this.#name || "Untitled Map";
    }

    /**
     * Updates the name of the map.
     */
    set name(value) {
        this.#name = value;
    }

    /**
     * Returns true if there is a wall that will block VroomBot at (x, y).
     *
     * @param {number} x - The x coordinate (the column in the map.)
     * @param {number} y - The y coordinate (the row in the map.)
     *
     * @return {boolean} Returns true only if the tile is a wall of some kind.
     */
    isWall(x, y) {
        // Is not a valid x coordinate
        if (x < 0 || x >= this.#width) {
            return true;
        }

        // Is not a valid y coordinate
        if (y < 0 || y >= this.#height) {
            return true;
        }

        if ((this.#rows[x][y] || 'x') == 'x') {
            return true;
        }

        return false;
    }

    /**
     * Returns true if the bot can move into the tile at (x, y).
     *
     * @param {number} x - The x coordinate (the column in the map.)
     * @param {number} y - The y coordinate (the row in the map.)
     *
     * @return {boolean} Returns true only if the tile is an occupiable space.
     */
    isTraversable(x, y) {
        return !this.isWall(x, y);
    }

    /**
     * Retrieves the tile at the given coordinate.
     *
     * @param {number} x - The x coordinate (the column in the map.)
     * @param {number} y - The y coordinate (the row in the map.)
     *
     * @return {str} The tile type. 'x' for a wall, ' ' for a cleared space,
     *               '.' for a dirty space.
     */
    get(x, y) {
        return this.#rows[x][y];
    }

    /**
     * Sets the tile at the given coordinate as the given tile type.
     *
     * @param {number} x - The x coordinate (the column in the map.)
     * @param {number} y - The y coordinate (the row in the map.)
     * @param {str} type - The tile type. 'x' for a wall, ' ' for a cleared space,
     *                     '.' for a dirty space.
     */
    set(x, y, type) {
        this.#currentTotals[this.#rows[x][y]]--;
        this.#rows[x][y] = type;
        this.#currentTotals[type] = (this.#currentTotals[type] || 0) + 1;

        // Determine if we met the win condition
        let ret = true;
        Object.keys(this.#goal).forEach( (type) => {
            let percent = this.#goal[type];
            if (this.#totals[type]) {
                let current = this.#currentTotals[type];
                let total = this.#totals[type];
                if (((current / total) * 100) > percent) {
                    ret = false;
                }
            }
        });
        this.#victory = this.#victory || ret;
    }

    /**
     * Resets map data back to its original form.
     */
    reset() {
        this.#currentTotals = Object.assign({}, this.#totals);
        this.#rows = [];
        this.#defeat = false;
        this.#victory = false;
        this.#original.forEach( (line) => {
            this.#rows.push(line.split(''));
        });
    }

    /**
     * Returns the coordinates for a random open tile.
     *
     * @return {object} An object with two fields: x and y, containing the
     *                  coordinates to an open tile.
     */
    randomTraversable() {
        // Find all traversible tiles
        let traversible = [];
        for (let y = 0; y < this.height; y++) {
            for (let x = 0; x < this.width; x++) {
                if (this.isTraversable(x, y)) {
                    traversible.push([x, y]);
                }
            }
        }

        // Randomly choose one
        let index = Math.round(Math.random() * traversible.length);
        return {
            x: traversible[index][0],
            y: traversible[index][1]
        };
    }

    /**
     * Exports the Map as a text document.
     *
     * @return {string} The text document representing the Map.
     */
    exportText() {
        return `# name: ${this.name}\n` + this.#rows.map( (row) => { return row.join(''); }).join("\n");
    }

    /**
     * Updates the Map with the given data.
     */
    #importText(data) {
        this.#width = 0;
        this.#height = 0;
        this.#original = [];
        this.#goal = {};
        this.#totals = {};

        data.trim().split("\n").forEach( (line, y) => {
            // Get rid of line comments
            let parts = line.split("#", 2);
            line = parts[0].toLowerCase().trim();
            let comment = (parts[1] || "").trim();
            if (comment.startsWith("name:")) {
                // Set name
                this.#name = comment.substring(5).trim();
            }
            if (!line) {
                return;
            }

            this.#original.push(line);
            if (line.length > this.#width) {
                this.#width = line.length;
            }
            this.#height++;

            line.split('').forEach( (tile) => {
                this.#totals[tile] = (this.#totals[tile] || 0) + 1;
            });
        });

        if (Object.keys(this.#goal).length == 0) {
            this.#goal = { '.': 0.0 };
        }

        this.reset();
    }

    /**
     * Generates and returns a new Map object based on the map data at the URL.
     *
     * @param {string} url - The URL for the map's data.
     *
     * @return {Promise} A promise providing the instantiated Map based on that data.
     */
    static fromURL(url) {
        return new Promise( (resolve, reject) => {
            fetch(url, {
            }).then( (response) => {
                return response.text();
            }).then( (text) => {
                resolve(new Map({ data: text }));
            }).catch( (error) => {
                reject(error);
            });
        });
    }
};

/**
 * The default VroomBot map.
 */
Map.DEFAULT_MAP = `
# name: The 45th Degree
xxxxxxxxxxxxxxxxxxxxxxxxx
xxxxxxxxxxxx.xxxxxxxxxxxx
xxxxxxxxxxx...xxxxxxxxxxx
xxxxxxxxxx.....xxxxxxxxxx
xxxxxxxxx.......xxxxxxxxx
xxxxxxxx.........xxxxxxxx
xxxxxxx...........xxxxxxx
xxxxxx.............xxxxxx
xxxxx...............xxxxx
xxxx.................xxxx
xxx...................xxx
xx.....................xx
x.......................x
xx.....................xx
xxx...................xxx
xxxx.................xxxx
xxxxx...............xxxxx
xxxxxx.............xxxxxx
xxxxxxx...........xxxxxxx
xxxxxxxx.........xxxxxxxx
xxxxxxxxx.......xxxxxxxxx
xxxxxxxxxx.....xxxxxxxxxx
xxxxxxxxxxx...xxxxxxxxxxx
xxxxxxxxxxxx.xxxxxxxxxxxx
xxxxxxxxxxxxxxxxxxxxxxxxx`

/**
 * The options for the Map object.
 * @typedef {Object} MapOptions
 * @property {string} data - A string containing the map data to use to create the map.
 */
