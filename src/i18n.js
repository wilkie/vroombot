"use strict";

import i18n from 'i18n-for-browser';

export function format(string, ...args) {
    return string.replace(/{(\d+)}/g, function(match, number) { 
        return typeof args[number] != 'undefined'
            ? args[number]
            : match
        ;
    });
}

i18n.configure({
    locales: {
        'en': {
            // Rules Editor
            'state': 'state',  // State header
            'if':    'if',     // Rule if section
            'else':  'else',   // Rule else-if section
            'then':  'then',   // Rule then section

            // Menus

            // Map Files
            'map-files': 'Map File Access',
            'map-open': 'Open Map',
            'map-save': 'Save Map',

            // Map Speed
            'speed': 'Speed',
            'speed-step': 'Step Once',
            'speed-slow': 'Slow',
            'speed-normal': 'Normal',
            'speed-fast': 'Fast',

            // Rules Files
            'rules-files': 'Rule File Access',
            'rules-open': 'Open Rules',
            'rules-save': 'Save Rules',

            // Rule Editing
            'rule-modify-sense': 'Update This Sense',
            'rule-clear-sense': 'Remove this sense rule',
            'rule-sense-N': 'Sense a North Wall',
            'rule-sense-n': 'Sense North is Empty',
            'rule-sense-S': 'Sense a South Wall',
            'rule-sense-s': 'Sense South is Empty',
            'rule-sense-E': 'Sense a East Wall',
            'rule-sense-e': 'Sense East is Empty',
            'rule-sense-W': 'Sense a West Wall',
            'rule-sense-w': 'Sense West is Empty',

            'rule-modify-action': 'Update the Action',
            'rule-action-N': 'Go North',
            'rule-action-W': 'Go West',
            'rule-action-E': 'Go East',
            'rule-action-S': 'Go South',

            'rule-modify-state': 'Update Destination State',
            'rule-switch-state': 'Go To State {0}',
        }
    }
});
